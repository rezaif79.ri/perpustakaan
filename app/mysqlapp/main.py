import models as md

db = md.session
table = md.customers

def showUsers():
    try:
        result = db.query(table).all()
        return result
    except Exception as e:
        print(e)

def showUserById(id):
    try:
        result = db.query(table).filter(table.userid==id).one()
        return result
    except Exception as e:
        print(e)

def insertUser(params):
    try:
        db.add(table(**params))
        db.commit()
        print("Data Insert Success")
    except Exception as e:
        print(e)

def updateUserById(id, params):
    try:
        result = db.query(table).filter(table.userid==id).one()
        result.username = params["username"]
        result.namadepan = params["namadepan"]
        result.namabelakang = params["namabelakang"]
        result.email = params["email"]
        db.commit()
        print("Data Update Success")
    except Exception as e:
        print(e)

def deleteUserById(id):
    try:
        result = db.query(table).filter(table.userid==id).one()
        print(result.userid , result.username, result.namadepan, result.namabelakang, result.email)
        db.delete(result)
        db.commit()
        print("Data Deleted")
    except Exception as e:
        print(e)
    


if __name__ == '__main__':
    
    data = {
    "username" : "rezaif",
    "namadepan" : "Reza",
    "namabelakang" : "Izzan Fadhila",
    "email" : "rezaif79.ri@gmail.com",
    }

    data_baru = {
        "username" : "mr_reza", # Username baru
        "namadepan" : "Reza",
        "namabelakang" : "Izzan Fadhila",
        "email" : "rezaif79.ri@gmail.com",
    }

    
    #COMMENT AND UNCOMMENT THE STATEMENT BELOW THIS COMMENT -RezaIF

    # hasil = showUsers()    
    # for data in hasil:
    #     print(data.userid , data.username, data.namadepan, data.namabelakang, data.email)

    # hasil = showUserById(2)
    # print(hasil.userid , hasil.username, hasil.namadepan, hasil.namabelakang, hasil.email)
 
    # insertUser(data)
     
    # updateUserById(26, data_baru)
    
    # deleteUserById(26)