import models
import re

docs = models.Books

def showBooks():
    try:
        for doc in docs.objects:
            print (doc.nama, doc.pengarang, doc.tahun_terbit, doc.genre, sep=", ")
    except Exception as e:
        print(e)

def showBookByName(judul):
    try:
        cari = re.compile("^{0}".format(judul))
        hasil = docs.objects(nama = cari)
        print(hasil.first().to_json())
        for doc in hasil:
            print (doc.nama, doc.pengarang, doc.tahun_terbit, doc.genre, sep=", ")
        
    except Exception as e:
        print(e)

def insertBook(data):
    try:
        docs(**data).save()
        print("Data Saved!")
    except Exception as e:
        print(e)

def updateBookByName(judul, **data_baru):
    try:
        cari = re.compile("^{0}".format(judul))
        hasil = docs.objects(nama = cari).first()
        print(hasil.to_json())

        hasil.nama = data_baru["nama"] 
        hasil.pengarang = data_baru["pengarang"]
        hasil.tahun_terbit = data_baru["tahun_terbit"]
        hasil.genre = data_baru["genre"]

        hasil.save()
        print("Data Updated!")

    except Exception as e:
        print(e)

def deleteBookByName(judul):
    try:
        cari = re.compile("^{0}".format(judul))
        hasil = docs.objects(nama = cari).first()
        print(hasil.to_json())

        hasil.delete()
        print("Data Deleted")
    except Exception as e:
        print(e)






if __name__ == '__main__':

    data = {
        "nama" : 'Buku Pelajaran - Pendidikan Olahraga Uhuy',
        "pengarang" : 'Markus Ovopulensis', 
        "tahun_terbit" : '2020',
        "genre" : "Education"
    }

    data_baru = {
        "nama" : 'Buku Pelajaran - Matematika',
        "pengarang" : 'Joni Marsupilami', 
        "tahun_terbit" : '2012',
        "genre" : "Education"
    }
    # insertBook(data)

    # updateBookByName("Buku Pelajaran", **data_baru)

    # showBooks()
    # deleteBookByName("Buku Pelajaran ")
    showBookByName("Buku Pelajaran ") 