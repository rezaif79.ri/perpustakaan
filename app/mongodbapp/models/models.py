from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument

connection = connect(db="perpustakaan", host="localhost", port=27017)

if connection:
    print("MongoDB Connected")

class Books(Document):
    nama = StringField(required=True, max_length=70)
    pengarang = StringField(required=True, max_length=70)
    tahun_terbit = StringField(required=True, max_length=5)
    genre = StringField(required=True, max_length=20)
